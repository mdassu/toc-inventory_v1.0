import { Component, Input, OnInit, Output, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { SortType } from "@swimlane/ngx-datatable";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";
import { ToastrService } from "ngx-toastr";
import { IBTTService } from "src/app/_services/ibtt.service";
import { DataTableDirective } from "angular-datatables";
import { EventEmitter } from "@angular/core";

@Component({
  selector: "app-records-table",
  templateUrl: "./records-table.component.html",
  styleUrls: ["./records-table.component.css"],
})
export class RecordsTableComponent implements OnInit {
  @Output() chartModel: EventEmitter<any> = new EventEmitter();
  @ViewChild(DataTableDirective, { static: false })
  datatableElement: DataTableDirective;
  @ViewChild("recordTable", { static: true }) table;
  @Input() columns: Array<string>;
  @Input() rows: Array<string>;
  @Input() tableName: string;
  config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  loadingIndicator: true;
  selected: Array<string> = [];
  hideColumn: Object = {};
  intialRows: Array<string> = [];
  temp: Array<string> = [];
  Math: any = Math;
  rowWidth: Array<any> = [
    { modalStoneName: "110321356", saleRanking: "1", originStock: "15" },
    { modalStoneName: "110321357", saleRanking: "2", originStock: "35" },
    { modalStoneName: "110321358", saleRanking: "3", originStock: "45" },
    { modalStoneName: "110321359", saleRanking: "4", originStock: "95" },
    { modalStoneName: "110321360", saleRanking: "5", originStock: "115" },
  ];
  donerrowData: any;
  clusterDonerData: any;
  lockedWindow: any;
  finalRecommend: Array<any> = [];
  replenishNeeded: Object = {};
  selectedColumn: string = "status";
  rowsLimit: Number = 10;
  rowsLimitList: Array<any> = [
    { value: 10 },
    { value: 20 },
    { value: 30 },
    { value: 50 },
    { value: 100 },
  ];
  SortType: SortType;
  dtOptions: any = {};
  dataColumns: Array<any> = [];
  dataRows: Array<any> = [];
  constructor(
    private modalService: NgbModal,
    private ibttServices: IBTTService,
    private toastService: ToastrService
  ) {}
  ngOnInit(): void {
    this.columns.map((item) => {
      this.hideColumn[item["name"]] = true;
      this.dataColumns.push({
        title: item["name"],
        data: item["element"],
      });
    });
    this.rows.map((item) => {
      var dataJson = {};
      this.columns.map((col) => {
        dataJson[col["element"]] = item[col["element"]];
      });
      this.dataRows.push(dataJson);
    });
    this.intialRows = this.rows;
    this.dtOptions = {
      // columns: this.dataColumns,buttons: ["colvis"],
      dom: "lBfrtip",
      buttons: [
        {
          extend: "colvis",
          columns: ":gt(1)",
        },
      ],
      columnDefs: [
        {
          targets: [0, 1], // column index (start from 0)
          orderable: false, // set orderable false for selected columns
        },
      ],
    };
  }

  ngAfterViewInit(): void {
    // this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
    //   dtInstance.columns().every(function () {
    //     const that = this;
    //     $("input", this.footer()).on("keyup change", function () {
    //       console.log(that.search());
    //       console.log(this["value"]);
    //       if (that.search() !== this["value"]) {
    //         console.log("test");
    //         that.search(this["value"]).draw();
    //       }
    //     });
    //   });
    // });
  }

  changeColumnStatus(name) {
    this.hideColumn[name] = !this.hideColumn[name];
  }

  updatedefaultFilter(event) {
    const val = event.target.value.toLowerCase();
    this.rows = [...this.intialRows]; // and here you have to initialize it with your data
    this.temp = [...this.rows];
    // filter our data
    const temp = this.rows.filter((d) => {
      return d[this.selectedColumn].toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
    // Whenever the filter changes, always go back to the first page
  }

  openModal(content, rowData) {
    if (this.tableName == "depth") {
      if (rowData % 2 == 0) {
        this.chartModel.emit();
      } else {
        this.modalService.open(content, {
          windowClass: "animated fadeInDown",
          size: "sm",
        });
      }
    } else if (this.tableName == "width") {
      this.modalService.open(content, {
        windowClass: "animated fadeInDown",
        size: "lg",
      });
    } else if (this.tableName == "ibtt") {
      const cluster_Name = rowData.cluster_Name;
      const apiParam = {
        ClusterName: cluster_Name,
        EAN: rowData.sku,
      };
      this.donerrowData = rowData;
      this.ibttServices.getIbttDonordata(apiParam).subscribe((result: any) => {
        this.clusterDonerData = result.data;
        this.lockedWindow = this.modalService.open(content, {
          windowClass: "animated fadeInDown",
          size: "lg",
        });
      });
    }
  }

  getApproveRejectDepth(status) {
    const apiParam = {
      Is_Approve: status,
      Ids: String(this.donerrowData.id),
      FinalRecommend: this.finalRecommend,
    };
    this.ibttServices
      .getApproveRejectDepth(apiParam)
      .subscribe((result: any) => {
        // this.getIbttdata();
        this.lockedWindow.close();
        if (result.success == 1) {
          if (status == 1) {
            this.toastService.success(
              "Your request submitted successfully.",
              "Success!"
            );
          }
        } else {
          this.toastService.error("Something went wrong.", "Error!");
        }
      });
  }

  onCheckboxChangeFn(data, event) {
    var params = {
      Ids: "1,5,8",
      Is_Approve: 1,
      FinalRecommend: [
        {
          From_Location: "Bangalore",
          To_Location: "Pali",
          item: "1234567",
          Qty: 2,
          Priority: 5,
          Status: "Approved",
        },
        {
          From_Location: "Jaipur",
          To_Location: "Kota",
          item: "7777774",
          Qty: 7,
          Priority: 2,
          Status: "Approved",
        },
      ],
    };
    if (event.target.checked) {
      this.replenishNeeded[data.id] = 0;
      var json = {
        donnerId: data.id,
        From_Location: data.originStock,
        To_Location: data.location,
        item: data.sku,
        Qty: this.replenishNeeded[data.id],
        Priority: data.priority,
        Status: data.status,
      };
      this.finalRecommend.push(json);
    } else {
      delete this.replenishNeeded[data.id];
      this.finalRecommend.map((item, key) => {
        if (item.donnerId == data.id) {
          this.finalRecommend.splice(key, 1);
        }
      });
    }
  }

  changeReplenishNeeded(donnerId) {
    this.finalRecommend.map((item) => {
      if (item.donnerId == donnerId) {
        item.Qty = parseInt(this.replenishNeeded[donnerId]);
      }
    });
  }

  genArray(num) {
    return Array(parseInt(num) + 1);
  }
}
