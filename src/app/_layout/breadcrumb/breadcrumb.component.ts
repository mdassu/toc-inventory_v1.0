import { HeaderfiltersService } from "./../../_services/headerfilters.service";
import { Component, OnInit, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
@Component({
  selector: "app-breadcrumb",
  templateUrl: "./breadcrumb.component.html",
  styleUrls: ["./breadcrumb.component.css"],
})
export class BreadcrumbComponent implements OnInit {
  // type: Number = 1;
  constructor(
    private modalService: NgbModal,
    private toastService: ToastrService,
    private fb: FormBuilder,
    private headerFillters: HeaderfiltersService
  ) {}
  // viewAllFilter: FormGroup;
  disabled = false;
  ShowFilter = true;
  limitSelection = false;
  lockedWindow: any;
  channelData = [];
  partnerData = [];
  selectedCanneldata = [];
  selectedpartnerData = [];
  modelStatus = true;

  stLocationData = [];
  stLocationDataArray = [];

  stStoreData = [];
  stStoreDataArray = [];
  stBrandsData = [];
  stBrandsDataArray = [];
  stNameData = [];
  StateDataArray = [];

  brandData = [];
  stCodeData = [];
  productBrandData = [];
  productBrandDataArray = [];

  productdtData1 = [];
  selectedItems = [];
  regionData = [];
  stateData = [];
  genderData = [];
  categoryData = [];
  categoryDataArray = [];
  agCodeData = [];
  agCodeDataArray = [];
  subCategoryData = [];
  subcategoryDataArray = [];
  familyData = [];
  familyDataArray = [];
  settingData = [];
  settingDataArray = [];
  designData = [];
  designDataArray = [];
  caratData = [];
  caratDataArray = [];
  salePriceData = [];
  salePriceDataArray = [];
  modelIdData = [];
  modelIdDataArray = [];
  qualityData = [];
  qualityDataArray = [];
  typeData = [];
  typeDataArray = [];
  lastNameData = [];
  lastNameDataArray = [];
  seasonData = [];
  seasonDataArray = [];
  mrpData = [];
  mrpDataArray = [];
  articalNumberData = [];
  dropdownSettings: any = {};
  destroyOnHidestatus = true;

  //Selected data
  selectedChannelDatalist = [];
  channelDataListn = [];
  selectedpartnerDatalist = [];
  selectedregionDatalist = [];
  selectedStateDatalist = [];
  selectedLocationDatalist = [];
  selectedStoreCodeDatalist = [];
  selectedBrandsDatalist = [];

  //Product tab Selected data
  productBrandDataDatalist = [];
  genderDatalist = [];
  categoryDataList = [];
  agCodeDataList = [];
  subCategoryDatalist = [];
  familyDataList = [];
  settingDataList = [];
  designDataList = [];
  caratDataList = [];
  salePriceDataList = [];
  modelIdDataList = [];
  qualityDataList = [];
  typeDatalist = [];
  lastNameDatalist = [];
  seasonDatalist = [];
  mrpDatalist = [];
  articalNumberDatalist = [];
  filterName: String = "";
  filterList: Array<any> = [];
  filterCheck: any;
  selectedFilterId: Number;
  editFilterId: Number = 0;
  locRegionData = [];
  locStateData = [];
  locCityData = [];
  locStNameData = [];
  locStCodeData = [];
  proCatData = [];
  proSbCatData = [];
  proFamNameData = [];
  proSettingData = [];
  proDesignData = [];
  proCaratsData = [];
  proSlPriceData = [];
  proAgData = [];
  proModIdData = [];
  proQuaData = [];
  ngOnInit() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: "item_text",
      textField: "item_text",
      selectAllText: " All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 2,
      allowSearchFilter: this.ShowFilter,
    };
    this.setFilter();
    // this.getChanneldata();
    // this.getProductBrand();
    this.getCategorydata();
    this.getRegiondata();
    this.processBreadCrumbLinks();
    this.getFiltersList();
    // New Dropdowns
    this.proCatData = [
      { item_id: 1, item_text: "Category1" },
      { item_id: 2, item_text: "Category2" },
      { item_id: 3, item_text: "Category3" },
      { item_id: 4, item_text: "Category4" },
    ];
    this.proSbCatData = [
      { item_id: 1, item_text: "Sub Category1" },
      { item_id: 2, item_text: "Sub Category2" },
      { item_id: 3, item_text: "Sub Category3" },
      { item_id: 4, item_text: "Sub Category4" },
    ];
    this.proFamNameData = [
      { item_id: 1, item_text: "Family1" },
      { item_id: 2, item_text: "Family2" },
      { item_id: 3, item_text: "Family3" },
      { item_id: 4, item_text: "Family4" },
    ];
    this.proSettingData = [
      { item_id: 1, item_text: "Setting1" },
      { item_id: 2, item_text: "Setting2" },
      { item_id: 3, item_text: "Setting3" },
      { item_id: 4, item_text: "Setting4" },
    ];
    this.proDesignData = [
      { item_id: 1, item_text: "Design1" },
      { item_id: 2, item_text: "Design2" },
      { item_id: 3, item_text: "Design3" },
      { item_id: 4, item_text: "Design4" },
    ];
    this.proCaratsData = [
      { item_id: 1, item_text: "Carats1" },
      { item_id: 2, item_text: "Carats2" },
      { item_id: 3, item_text: "Carats3" },
      { item_id: 4, item_text: "Carats4" },
    ];
    this.proSlPriceData = [
      { item_id: 1, item_text: "5000" },
      { item_id: 2, item_text: "6000" },
      { item_id: 3, item_text: "4000" },
      { item_id: 4, item_text: "7000" },
    ];
    this.proAgData = [
      { item_id: 1, item_text: "AG1" },
      { item_id: 2, item_text: "AG2" },
      { item_id: 3, item_text: "AG3" },
      { item_id: 4, item_text: "AG4" },
    ];
    this.proModIdData = [
      { item_id: 1, item_text: "Modal1" },
      { item_id: 2, item_text: "Modal2" },
      { item_id: 3, item_text: "Modal3" },
      { item_id: 4, item_text: "Modal4" },
    ];
    this.proQuaData = [
      { item_id: 1, item_text: "Quality1" },
      { item_id: 2, item_text: "Quality2" },
      { item_id: 3, item_text: "Quality3" },
      { item_id: 4, item_text: "Quality4" },
    ];
    this.locRegionData = [
      { item_id: 1, item_text: "India" },
      { item_id: 2, item_text: "US" },
      { item_id: 3, item_text: "England" },
      { item_id: 4, item_text: "UAE" },
    ];
    this.locStateData = [
      { item_id: 1, item_text: "Rajasthan" },
      { item_id: 2, item_text: "California" },
      { item_id: 3, item_text: "London" },
      { item_id: 4, item_text: "Dubai" },
    ];
    this.locCityData = [
      { item_id: 1, item_text: "Jodhpur" },
      { item_id: 2, item_text: "Washington" },
      { item_id: 3, item_text: "London" },
      { item_id: 4, item_text: "Dubai" },
    ];
    this.locStNameData = [
      { item_id: 1, item_text: "Store1" },
      { item_id: 2, item_text: "Store2" },
      { item_id: 3, item_text: "Store3" },
      { item_id: 4, item_text: "Store4" },
    ];
    this.locStCodeData = [
      { item_id: 1, item_text: "Store1001" },
      { item_id: 2, item_text: "Store1002" },
      { item_id: 3, item_text: "Store1003" },
      { item_id: 4, item_text: "Store1004" },
    ];
  }

  setFilter() {
    if (localStorage.getItem("filterJson")) {
      var filterJson = JSON.parse(localStorage.getItem("filterJson"));
      this.selectedFilterId = parseInt(localStorage.getItem("filterId"));
      //Product
      // if (filterJson["productbrand"] != "") {
      //   this.productBrandDataDatalist = filterJson["productbrand"].split(",");
      //   this.productBrandDataArray = this.productBrandDataDatalist;
      //   this.getGenderdata();
      //   this.getCategorydata();
      // }
      // if (filterJson["gender"] != "") {
      //   this.genderDatalist = filterJson["gender"].split(",");
      // }
      if (filterJson["category"] != "") {
        this.categoryDataList = filterJson["category"].split(",");
        this.categoryDataArray = this.categoryDataList;
        this.getAgCodeData();
      }
      if (filterJson["agCode"] != "") {
        this.agCodeDataList = filterJson["agCode"].split(",");
        this.agCodeDataArray = this.agCodeDataList;
        this.getSubcategorydata();
      }
      if (filterJson["subcategory"] != "") {
        this.subCategoryDatalist = filterJson["subcategory"].split(",");
        this.subcategoryDataArray = this.subCategoryDatalist;
        this.getFamilyData();
      }
      if (filterJson["familyName"] != "") {
        this.familyDataList = filterJson["familyName"].split(",");
        this.familyDataArray = this.familyDataList;
        this.getSettingData();
      }
      if (filterJson["setting"] != "") {
        this.settingDataList = filterJson["setting"].split(",");
        this.settingDataArray = this.settingDataList;
        this.getDesignData();
      }

      if (filterJson["designNo"] != "") {
        this.designDataList = filterJson["designNo"].split(",");
        this.designDataArray = this.designDataList;
        this.getCaratData();
      }

      if (filterJson["carats"] != "") {
        this.caratDataList = filterJson["carats"].split(",");
        this.caratDataArray = this.caratDataList;
        this.getSelesPriceData();
      }

      if (filterJson["salePrice"] != "") {
        this.salePriceDataList = filterJson["salePrice"].split(",");
        this.salePriceDataArray = this.salePriceDataList;
        this.getModelIdData();
      }
      if (filterJson["modelId"] != "") {
        this.modelIdDataList = filterJson["modelId"].split(",");
        this.modelIdDataArray = this.modelIdDataList;
        this.getQualityData();
      }
      if (filterJson["quality"] != "") {
        this.qualityDataList = filterJson["quality"].split(",");
        this.qualityDataArray = this.qualityDataList;
      }
      // if (filterJson["type"] != "") {
      //   this.typeDatalist = filterJson["type"].split(",");
      //   this.typeDataArray = this.typeDatalist;
      //   this.getLastnamedata();
      // }
      // if (filterJson["lastname"] != "") {
      //   this.lastNameDatalist = filterJson["lastname"].split(",");
      //   this.lastNameDataArray = this.lastNameDatalist;
      //   this.getSeasondataList();
      // }
      // if (filterJson["season"] != "") {
      //   this.seasonDatalist = filterJson["season"].split(",");
      //   this.seasonDataArray = this.seasonDatalist;
      //   this.getMrpdatalist();
      // }
      // if (filterJson["mrp"] != "") {
      //   this.mrpDatalist = filterJson["mrp"].split(",");
      //   this.mrpDataArray = this.mrpDatalist;
      //   this.getarticalnoData();
      // }
      // if (filterJson["articalno"] != "") {
      //   this.articalNumberDatalist = filterJson["articalno"].split(",");
      // }

      // //Location
      // if (filterJson["channel"] != "") {
      //   this.selectedChannelDatalist = filterJson["channel"].split(",");
      // }
      // if (filterJson["partners"] != "") {
      //   this.selectedpartnerDatalist = filterJson["partners"].split(",");
      // }
      if (filterJson["region"] != "") {
        this.selectedregionDatalist = filterJson["region"].split(",");
      }
      if (filterJson["state"] != "") {
        this.selectedStateDatalist = filterJson["state"].split(",");
      }
      if (filterJson["city"] != "") {
        this.selectedLocationDatalist = filterJson["city"].split(",");
      }
      if (filterJson["storeCode"] != "") {
        this.selectedStoreCodeDatalist = filterJson["storeCode"].split(",");
      }
      // if (filterJson["locationbrand"] != "") {
      //   this.selectedBrandsDatalist = filterJson["locationbrand"].split(",");
      // }
      this.headerFillters.callFilterData.next(true);
    } else {
      this.productBrandDataDatalist = [];
      this.genderDatalist = [];
      this.categoryDataList = [];
      this.subCategoryDatalist = [];
      this.typeDatalist = [];
      this.lastNameDatalist = [];
      this.seasonDatalist = [];
      this.mrpDatalist = [];
      this.articalNumberDatalist = [];
      //Location

      this.selectedChannelDatalist = [];
      this.selectedpartnerDatalist = [];
      this.selectedregionDatalist = [];
      this.selectedStateDatalist = [];
      this.selectedLocationDatalist = [];
      this.selectedBrandsDatalist = [];

      this.headerFillters.callFilterData.next(true);
    }
  }

  onTabChange(item: any) {
    this.destroyOnHidestatus = false;
  }

  //Selected one Item
  onItemSelect(item: any, type = "") {
    if (type === "channel") {
      this.selectedCanneldata.push(item);

      this.selectedpartnerDatalist = this.selectedregionDatalist = this.selectedStateDatalist = this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.regionData = this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
      this.getPatnerdata();
    }

    // if (type === "partners") {
    //   this.selectedpartnerData.push(item);
    //   this.selectedregionDatalist = this.selectedStateDatalist = this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
    //   this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
    //   this.getRegiondata();
    // }

    if (type === "region") {
      this.StateDataArray.push(item);

      this.selectedStateDatalist = this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.stLocationData = this.stStoreData = this.stBrandsData = [];
      this.getStates();
    }

    if (type === "state") {
      this.stLocationDataArray.push(item);

      this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.stStoreData = this.stBrandsData = [];

      this.getLocation();
    }

    if (type === "location") {
      this.stStoreDataArray.push(item);

      this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.stBrandsData = [];

      this.getStore();
    }

    if (type === "store") {
      this.stBrandsDataArray.push(item);

      // this.selectedBrandsDatalist = [];
      // this.getBrands();
    }

    //Product Filter Options
    // if (type === "pbrand") {
    //   this.productBrandDataArray.push(item);
    //   this.genderDatalist = this.categoryDataList = this.subCategoryDatalist = this.subCategoryDatalist = this.typeDatalist = this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
    //   this.subCategoryData = this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

    //   this.getGenderdata();
    //   this.getCategorydata();
    // }

    if (type === "category") {
      this.categoryDataArray.push(item);
      this.agCodeDataList = this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      // this.getSubcategorydata();
      this.getAgCodeData();
    }

    if (type === "agCode") {
      this.agCodeDataArray.push(item);
      this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getSubcategorydata();
    }

    if (type === "subcategory") {
      this.subcategoryDataArray.push(item);
      this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getFamilyData();
    }

    if (type === "family") {
      this.familyDataArray.push(item);
      this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getSettingData();
    }

    if (type === "setting") {
      this.settingDataArray.push(item);
      this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getDesignData();
    }

    if (type === "design") {
      this.designDataArray.push(item);
      this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getCaratData();
    }

    if (type === "carat") {
      this.caratDataArray.push(item);
      this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getSelesPriceData();
    }

    if (type === "salePrice") {
      this.salePriceDataArray.push(item);
      this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getModelIdData();
    }

    if (type === "modelId") {
      this.modelIdDataArray.push(item);
      this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
      // this.getTypedata();
      this.getQualityData();
    }

    if (type === "quality") {
      this.qualityDataArray.push(item);
    }

    // if (type === "type") {
    //   this.typeDataArray.push(item);
    //   this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
    //   this.getLastnamedata();
    //   this.seasonData = this.articalNumberData = this.mrpData = [];
    // }

    // if (type === "lastname") {
    //   this.lastNameDataArray.push(item);
    //   this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
    //   this.getSeasondataList();
    //   this.mrpData = this.articalNumberData = [];
    // }
    // if (type === "season") {
    //   this.seasonDataArray.push(item);
    //   this.articalNumberDatalist = this.mrpDatalist = [];
    //   this.getMrpdatalist();
    //   this.articalNumberData = [];
    // }
    // if (type === "mrp") {
    //   this.mrpDataArray.push(item);
    //   this.articalNumberDatalist = [];
    //   this.getarticalnoData();
    // }
  }

  //Selected All Item

  onSelectAll(items: any, type = "") {
    // if (type === "channel") {
    //   this.selectedCanneldata = ["All"];

    //   this.selectedpartnerDatalist = this.selectedregionDatalist = this.selectedStateDatalist = this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];

    //   this.regionData = this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
    //   this.getPatnerdata();
    //   this.selectedCanneldata = this.channelData;
    // }
    // if (type === "partners") {
    //   this.selectedpartnerData = ["All"];
    //   this.selectedregionDatalist = this.selectedStateDatalist = this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
    //   this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
    //   this.getRegiondata();
    //   this.selectedpartnerData = this.partnerData;
    // }
    if (type === "region") {
      this.StateDataArray = ["All"];
      this.selectedStateDatalist = this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.stLocationData = this.stStoreData = this.stBrandsData = [];
      this.getStates();
      this.StateDataArray = this.regionData;
    }
    if (type === "state") {
      this.stLocationDataArray = ["All"];
      this.selectedLocationDatalist = this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.stStoreData = this.stBrandsData = [];
      this.getLocation();
      this.stLocationDataArray = this.stateData;
    }

    if (type === "location") {
      this.stStoreDataArray = ["All"];

      this.selectedStoreCodeDatalist = this.selectedBrandsDatalist = [];
      this.stBrandsData = [];
      this.getStore();
      this.stStoreDataArray = this.stLocationData;
    }
    if (type === "store") {
      this.stBrandsDataArray = ["All"];
      this.selectedBrandsDatalist = [];
      this.getBrands();
      this.stBrandsDataArray = this.stStoreData;
    }

    //Product Filter Options

    // if (type === "pbrand") {
    //   this.productBrandDataArray = ["All"];
    //   this.getGenderdata();
    //   this.getCategorydata();
    //   this.productBrandDataArray = this.productBrandData;
    //   this.genderDatalist = this.categoryDataList = this.subCategoryDatalist = this.typeDatalist = this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
    //   this.subCategoryData = this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    // }

    if (type === "category") {
      this.categoryDataArray = ["All"];
      this.agCodeDataList = this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getAgCodeData();
      this.categoryDataArray = this.categoryData;
    }

    if (type === "agCode") {
      this.agCodeDataArray = ["All"];
      this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getSubcategorydata();
      this.agCodeDataArray = this.agCodeData;
    }

    if (type === "subcategory") {
      this.subcategoryDataArray = ["All"];
      this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getFamilyData();
      this.subcategoryDataArray = this.subCategoryData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "family") {
      this.familyDataArray = ["All"];
      this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getSettingData();
      this.familyDataArray = this.familyData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "setting") {
      this.settingDataArray = ["All"];
      this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getDesignData();
      this.settingDataArray = this.settingData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "design") {
      this.designDataArray = ["All"];
      this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getCaratData();
      this.designDataArray = this.designData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "carat") {
      this.caratDataArray = ["All"];
      this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getSelesPriceData();
      this.caratDataArray = this.caratData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "salePrice") {
      this.salePriceDataArray = ["All"];
      this.modelIdDataList = this.qualityDataList = [];
      this.getModelIdData();
      this.salePriceDataArray = this.salePriceData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "modelId") {
      this.modelIdDataArray = ["All"];
      this.qualityDataList = [];
      this.getQualityData();
      this.modelIdDataArray = this.modelIdData;
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }
    if (type === "quality") {
      this.qualityDataArray = ["All"];
      this.qualityDataArray = this.qualityData;
    }

    if (type === "type") {
      this.typeDataArray = ["All"];
      this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.getLastnamedata();
      this.typeDataArray = this.typeData;
      this.seasonData = this.mrpData = this.articalNumberData = [];
    }
    if (type === "lastname") {
      this.lastNameDataArray = ["All"];
      this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.getSeasondataList();
      this.lastNameDataArray = this.lastNameData;
      this.mrpData = this.articalNumberData = [];
    }
    if (type === "season") {
      this.seasonDataArray = ["All"];
      this.articalNumberDatalist = this.mrpDatalist = [];
      this.getMrpdatalist();
      this.seasonDataArray = this.seasonData;
      this.articalNumberData = [];
    }
    if (type === "mrp") {
      this.mrpDataArray = ["All"];
      this.articalNumberDatalist = [];
      this.getarticalnoData();
      this.mrpDataArray = this.mrpData;
    }
  }

  onItemDeSelect(items: any, type = "") {
    if (type === "channel") {
      this.selectedpartnerDatalist = [];

      const index = this.selectedCanneldata.indexOf(items);
      if (index > -1) {
        this.selectedCanneldata.splice(index, 1);
        this.selectedpartnerDatalist = [];
        this.selectedregionDatalist = [];
        this.selectedStateDatalist = [];
        this.selectedLocationDatalist = [];
        this.selectedStoreCodeDatalist = [];
        this.selectedBrandsDatalist = [];
        this.regionData = this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
        this.getPatnerdata();
      }
    }
    if (type === "partners") {
      const index = this.selectedpartnerData.indexOf(items);
      if (index > -1) {
        this.selectedpartnerData.splice(index, 1);
        this.selectedregionDatalist = [];
        this.selectedStateDatalist = [];
        this.selectedLocationDatalist = [];
        this.selectedStoreCodeDatalist = [];
        this.selectedBrandsDatalist = [];
        this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
        this.getRegiondata();
      }
    }
    if (type === "region") {
      const index = this.StateDataArray.indexOf(items);
      if (index > -1) {
        this.StateDataArray.splice(index, 1);

        this.selectedStateDatalist = [];
        this.selectedLocationDatalist = [];
        this.selectedStoreCodeDatalist = [];
        this.selectedBrandsDatalist = [];
        this.stLocationData = this.stStoreData = this.stBrandsData = [];
        this.getStates();
      }
    }
    if (type === "state") {
      const index = this.stLocationDataArray.indexOf(items);
      if (index > -1) {
        this.stLocationDataArray.splice(index, 1);
        this.selectedLocationDatalist = [];
        this.selectedStoreCodeDatalist = [];
        this.selectedBrandsDatalist = [];
        this.stStoreData = this.stBrandsData = [];
        this.getLocation();
      }
    }

    if (type === "location") {
      const index = this.stStoreDataArray.indexOf(items);
      if (index > -1) {
        this.stStoreDataArray.splice(index, 1);
        this.selectedStoreCodeDatalist = [];
        this.selectedBrandsDatalist = [];
        this.stBrandsData = [];
        this.getStore();
      }
    }

    if (type === "store") {
      const index = this.stBrandsDataArray.indexOf(items);
      if (index > -1) {
        this.stBrandsDataArray.splice(index, 1);
        this.selectedBrandsDatalist = [];
        this.getBrands();
      }
    }

    if (type === "pbrand") {
      const index = this.productBrandDataArray.indexOf(items);
      if (index > -1) {
        this.productBrandDataArray.splice(index, 1);
      }

      this.genderDatalist = this.categoryDataList = this.subCategoryDatalist = this.typeDatalist = this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.subCategoryData = this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getGenderdata();
      this.getCategorydata();
    }

    if (type === "category") {
      const index = this.categoryDataArray.indexOf(items);
      if (index > -1) {
        this.categoryDataArray.splice(index, 1);
      }

      this.getAgCodeData();
      this.agCodeDataList = this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "agCode") {
      const index = this.agCodeDataArray.indexOf(items);
      if (index > -1) {
        this.agCodeDataArray.splice(index, 1);
      }

      this.getSubcategorydata();
      this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "subcategory") {
      const index = this.subcategoryDataArray.indexOf(items);
      if (index > -1) {
        this.subcategoryDataArray.splice(index, 1);
      }
      this.getFamilyData();
      this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "family") {
      const index = this.familyDataArray.indexOf(items);
      if (index > -1) {
        this.familyDataArray.splice(index, 1);
      }
      this.getSettingData();
      this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "setting") {
      const index = this.settingDataArray.indexOf(items);
      if (index > -1) {
        this.settingDataArray.splice(index, 1);
      }
      this.getDesignData();
      this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "desgin") {
      const index = this.designDataArray.indexOf(items);
      if (index > -1) {
        this.designDataArray.splice(index, 1);
      }
      this.getCaratData();
      this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "carat") {
      const index = this.caratDataArray.indexOf(items);
      if (index > -1) {
        this.caratDataArray.splice(index, 1);
      }
      this.getSelesPriceData();
      this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "salePrice") {
      const index = this.salePriceDataArray.indexOf(items);
      if (index > -1) {
        this.salePriceDataArray.splice(index, 1);
      }
      this.getModelIdData();
      this.modelIdDataList = this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "modelId") {
      const index = this.modelIdDataArray.indexOf(items);
      if (index > -1) {
        this.modelIdDataArray.splice(index, 1);
      }
      this.getQualityData();
      this.qualityDataList = [];
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "quality") {
      const index = this.qualityDataArray.indexOf(items);
      if (index > -1) {
        this.qualityDataArray.splice(index, 1);
      }
    }

    if (type === "type") {
      const index = this.typeDataArray.indexOf(items);
      if (index > -1) {
        this.typeDataArray.splice(index, 1);
      }
      this.getLastnamedata();
      this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.seasonData = this.articalNumberData = this.mrpData = [];
    }
    if (type === "lastname") {
      const index = this.lastNameDataArray.indexOf(items);
      if (index > -1) {
        this.lastNameDataArray.splice(index, 1);
      }
      this.getSeasondataList();
      this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.mrpData = this.articalNumberData = [];
    }
    if (type === "season") {
      const index = this.seasonDataArray.indexOf(items);
      if (index > -1) {
        this.seasonDataArray.splice(index, 1);
      }
      this.getMrpdatalist();
      this.articalNumberDatalist = this.mrpDatalist = [];
      this.articalNumberData = [];
    }
    if (type === "mrp") {
      const index = this.mrpDataArray.indexOf(items);
      if (index > -1) {
        this.mrpDataArray.splice(index, 1);
      }
      this.getarticalnoData();
      this.articalNumberDatalist = [];
    }
  }
  onDeSelectAllItems(items: any, type = "") {
    if (type === "channel") {
      this.selectedCanneldata = [];
      this.selectedpartnerDatalist = [];
      this.selectedregionDatalist = [];
      this.selectedStateDatalist = [];
      this.selectedLocationDatalist = [];
      this.selectedStoreCodeDatalist = [];
      this.selectedBrandsDatalist = [];
      this.regionData = this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
      this.getPatnerdata();
    }
    if (type === "partners") {
      this.selectedregionDatalist = [];
      this.selectedStateDatalist = [];
      this.selectedLocationDatalist = [];
      this.selectedStoreCodeDatalist = [];
      this.selectedBrandsDatalist = [];
      this.stateData = this.stLocationData = this.stStoreData = this.stBrandsData = [];
      this.getRegiondata();
    }
    if (type === "region") {
      this.StateDataArray = [];
      this.selectedStateDatalist = [];
      this.selectedLocationDatalist = [];
      this.selectedStoreCodeDatalist = [];
      this.selectedBrandsDatalist = [];
      this.stLocationData = this.stStoreData = this.stBrandsData = [];
      this.getStates();
    }
    if (type === "state") {
      this.stLocationDataArray = [];
      this.selectedLocationDatalist = [];
      this.selectedStoreCodeDatalist = [];
      this.selectedBrandsDatalist = [];
      this.stStoreData = this.stBrandsData = [];
      this.getLocation();
    }

    if (type === "location") {
      this.stStoreDataArray = [];
      this.selectedStoreCodeDatalist = [];
      this.selectedBrandsDatalist = [];
      this.stBrandsData = [];
      this.getStore();
    }
    if (type === "store") {
      this.stBrandsDataArray = [];
      this.selectedBrandsDatalist = [];
      this.getBrands();
    }

    if (type === "pbrand") {
      this.productBrandDataArray = [];
      this.genderDatalist = this.categoryDataList = this.subCategoryDatalist = this.typeDatalist = this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];

      this.subCategoryData = this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getGenderdata();
      this.getCategorydata();
    }

    if (type === "category") {
      this.categoryDataArray = [];
      this.agCodeDataList = this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getAgCodeData();
    }

    if (type === "agCode") {
      this.agCodeDataArray = [];
      this.subCategoryDatalist = this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.typeData = this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getSubcategorydata();
    }

    if (type === "subcategory") {
      this.subcategoryDataArray = [];
      this.familyDataList = this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getFamilyData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "family") {
      this.familyDataArray = [];
      this.settingDataList = this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getSettingData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "setting") {
      this.settingDataArray = [];
      this.designDataList = this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getDesignData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "design") {
      this.designDataArray = [];
      this.caratDataList = this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getCaratData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "carat") {
      this.caratDataArray = [];
      this.salePriceDataList = this.modelIdDataList = this.qualityDataList = [];
      this.getSelesPriceData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "salePrice") {
      this.salePriceDataArray = [];
      this.modelIdDataList = this.qualityDataList = [];
      this.getModelIdData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "modelId") {
      this.modelIdDataArray = [];
      this.qualityDataList = [];
      this.getQualityData();
      this.lastNameData = this.seasonData = this.articalNumberData = this.mrpData = [];
    }

    if (type === "quality") {
      this.qualityDataArray = [];
    }

    if (type === "type") {
      this.typeDataArray = [];
      this.lastNameDatalist = this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.seasonData = this.articalNumberData = this.mrpData = [];

      this.getLastnamedata();
    }
    if (type === "lastname") {
      this.lastNameDataArray = [];
      this.seasonDatalist = this.articalNumberDatalist = this.mrpDatalist = [];
      this.mrpData = this.articalNumberData = [];

      this.getSeasondataList();
    }

    if (type === "season") {
      this.seasonDataArray = [];
      this.articalNumberDatalist = this.mrpDatalist = [];

      this.getMrpdatalist();
      this.articalNumberData = [];
    }
    if (type === "mrp") {
      this.mrpDataArray = [];
      this.articalNumberDatalist = [];
      this.getarticalnoData();
    }
  }
  toogleShowFilter() {
    this.ShowFilter = !this.ShowFilter;
    this.dropdownSettings = Object.assign({}, this.dropdownSettings, {
      allowSearchFilter: this.ShowFilter,
    });
  }

  handleLimitSelection() {
    if (this.limitSelection) {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, {
        limitSelection: 2,
      });
    } else {
      this.dropdownSettings = Object.assign({}, this.dropdownSettings, {
        limitSelection: null,
      });
    }
  }
  private processBreadCrumbLinks() {}

  LargeModel(LargeModelContent) {
    this.editFilterId = 0;
    this.setFilter();
    this.lockedWindow = this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown filters-modal",
      size: "lg",
    });
  }
  SmallModel(SmallModelContent, type) {
    if (
      this.selectedChannelDatalist.length > 0 ||
      this.selectedpartnerDatalist.length > 0 ||
      this.selectedregionDatalist.length > 0 ||
      this.selectedStateDatalist.length > 0 ||
      this.selectedLocationDatalist.length > 0 ||
      this.selectedBrandsDatalist.length > 0 ||
      this.productBrandDataDatalist.length > 0 ||
      this.genderDatalist.length > 0 ||
      this.categoryDataList.length > 0 ||
      this.subCategoryDatalist.length > 0 ||
      this.typeDatalist.length > 0 ||
      this.lastNameDatalist.length > 0 ||
      this.seasonDatalist.length > 0 ||
      this.mrpDatalist.length > 0 ||
      this.articalNumberDatalist.length > 0
    ) {
      this.modalService.open(SmallModelContent, {
        windowClass: "animated fadeInDown saved-modal",
        size: "sm",
      });
    } else if (type == "list") {
      this.modalService.open(SmallModelContent, {
        windowClass: "animated fadeInDown saved-modal",
        size: "sm",
      });
    } else {
      this.toastService.warning("Select at least one field", "Warning!");
    }
  }

  getChanneldata() {
    const apiParam = {};
    this.headerFillters.getChannel(apiParam).subscribe((result: any) => {
      if (result.data.length >= 0) {
        this.channelData = result.data;
      } else {
        this.partnerData = [];
        this.regionData = [];
      }
    });
  }

  getPatnerdata() {
    const apiParam = {
      keyName: this.selectedCanneldata.join(","),
    };

    this.headerFillters.getPatners(apiParam).subscribe((result: any) => {
      this.partnerData = result.data;
    });
  }

  getRegiondata() {
    const apiParam = {
      keyName: "All",
    };

    this.headerFillters.Getregiondata(apiParam).subscribe((result: any) => {
      this.regionData = result.data;
    });
  }

  getLocation() {
    const apiParam = {
      keyName: this.stLocationDataArray.join(","),
    };
    this.headerFillters.getLocation(apiParam).subscribe((result: any) => {
      this.regionData = result.data;
      this.stLocationData = result.data;
    });
  }

  getStore() {
    const apiParam = {
      keyName: this.stStoreDataArray.join(","),
    };
    this.headerFillters.getStore(apiParam).subscribe((result: any) => {
      this.stStoreData = result.data;
    });
  }

  getBrands() {
    const apiParam = {
      keyName: this.stBrandsDataArray.join(","),
    };
    this.headerFillters.getBrands(apiParam).subscribe((result: any) => {
      this.stBrandsData = result.data;
    });
  }

  getStates() {
    const apiParam = {
      keyName: this.StateDataArray.join(","),
    };

    this.headerFillters.getStates(apiParam).subscribe((result: any) => {
      this.stateData = result.data;
    });
  }

  getProductBrand() {
    const apiParam = {};
    this.headerFillters.getPoducts(apiParam).subscribe((result: any) => {
      this.productBrandData = result.data;
    });
  }

  getGenderdata() {
    const apiParam = {
      keyName: this.productBrandDataArray.join(","),
    };
    this.headerFillters.getGenderData(apiParam).subscribe((result: any) => {
      this.genderData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  // getCategorydata() {
  //   const apiParam = {
  //     keyName: this.productBrandDataArray.join(","),
  //   };
  //   this.headerFillters.getCategoryData(apiParam).subscribe((result: any) => {
  //     this.categoryData = result.data.filter(function (el) {
  //       return el != null;
  //     });
  //   });
  // }

  getCategorydata() {
    const apiParam = {
      keyName: "All",
    };
    this.headerFillters.getCategoryData(apiParam).subscribe((result: any) => {
      this.categoryData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getAgCodeData() {
    const apiParam = {
      keyName: this.categoryDataArray.join(","),
    };
    this.headerFillters.getAgCodeData(apiParam).subscribe((result: any) => {
      this.agCodeData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  //Get All SubCategory
  getSubcategorydata() {
    const apiParam = {
      keyName: this.agCodeDataArray.join(","),
    };
    this.headerFillters
      .getSubcategorydata(apiParam)
      .subscribe((result: any) => {
        this.subCategoryData = result.data.filter(function (el) {
          return el != null;
        });
      });
  }

  getFamilyData() {
    const apiParam = {
      keyName: this.subcategoryDataArray.join(","),
    };
    this.headerFillters.getFamilyData(apiParam).subscribe((result: any) => {
      this.familyData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getSettingData() {
    const apiParam = {
      keyName: this.familyDataArray.join(","),
    };
    this.headerFillters.getSettingData(apiParam).subscribe((result: any) => {
      this.settingData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getDesignData() {
    const apiParam = {
      keyName: this.settingDataArray.join(","),
    };
    this.headerFillters.getDesignData(apiParam).subscribe((result: any) => {
      this.designData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getCaratData() {
    const apiParam = {
      keyName: this.designDataArray.join(","),
    };
    this.headerFillters.getCaratData(apiParam).subscribe((result: any) => {
      this.caratData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getSelesPriceData() {
    const apiParam = {
      keyName: this.caratDataArray.join(","),
    };
    this.headerFillters.getSalePriceData(apiParam).subscribe((result: any) => {
      this.salePriceData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getModelIdData() {
    const apiParam = {
      keyName: this.salePriceDataArray.join(","),
    };
    this.headerFillters.getModalIdData(apiParam).subscribe((result: any) => {
      this.modelIdData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getQualityData() {
    const apiParam = {
      keyName: this.modelIdDataArray.join(","),
    };
    this.headerFillters.getQualityData(apiParam).subscribe((result: any) => {
      this.qualityData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getTypedata() {
    const apiParam = {
      keyName: this.subcategoryDataArray.join(","),
    };
    this.headerFillters.getTypedata(apiParam).subscribe((result: any) => {
      this.typeData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getLastnamedata() {
    const apiParam = {
      keyName: this.typeDataArray.join(","),
    };
    this.headerFillters.getLastnamedata(apiParam).subscribe((result: any) => {
      this.lastNameData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getSeasondataList() {
    const apiParam = {
      keyName: this.lastNameDataArray.join(","),
    };
    this.headerFillters.getSeasondataList(apiParam).subscribe((result: any) => {
      this.seasonData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getMrpdatalist() {
    const apiParam = {
      keyName: this.seasonDataArray.join(","),
    };
    this.headerFillters.getMrpdatalist(apiParam).subscribe((result: any) => {
      this.mrpData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  getarticalnoData() {
    const apiParam = {
      keyName: this.mrpDataArray.join(","),
    };
    this.headerFillters.getarticalnoData(apiParam).subscribe((result: any) => {
      this.articalNumberData = result.data.filter(function (el) {
        return el != null;
      });
    });
  }

  @Input() breadcrumb: object;

  getDataFromFilter() {
    // this.headerFillters.selectedChannelDatalist = this.selectedChannelDatalist;
    // this.headerFillters.selectedpartnerDatalist = this.selectedpartnerDatalist;
    this.headerFillters.selectedregionDatalist = this.selectedregionDatalist;
    this.headerFillters.selectedStateDatalist = this.selectedStateDatalist;
    this.headerFillters.selectedStoreCodeDatalist = this.selectedStoreCodeDatalist;
    // this.headerFillters.selectedBrandsDatalist = this.selectedBrandsDatalist;
    this.headerFillters.selectedLocationDatalist = this.selectedLocationDatalist;

    // this.headerFillters.productBrandDataDatalist = this.productBrandDataDatalist;
    // this.headerFillters.genderDatalist = this.genderDatalist;
    // this.headerFillters.typeDatalist = this.typeDatalist;
    // this.headerFillters.lastNameDatalist = this.lastNameDatalist;
    // this.headerFillters.seasonDatalist = this.seasonDatalist;
    // this.headerFillters.mrpDatalist = this.mrpDatalist;
    // this.headerFillters.articalNumberDatalist = this.articalNumberDatalist;

    this.headerFillters.categoryDataList = this.categoryDataList;
    this.headerFillters.agCodeDataList = this.agCodeDataList;
    this.headerFillters.subCategoryDatalist = this.subCategoryDatalist;
    this.headerFillters.familyDataList = this.familyDataList;
    this.headerFillters.settingDataList = this.settingDataList;
    this.headerFillters.designDataList = this.designDataList;
    this.headerFillters.caratDataList = this.caratDataList;
    this.headerFillters.salePriceDataList = this.salePriceDataList;
    this.headerFillters.modelIdDataList = this.modelIdDataList;
    this.headerFillters.qualityDataList = this.qualityDataList;

    const brandsCode = [];

    this.selectedStoreCodeDatalist.forEach(function (task) {
      let n = task.substring(task.lastIndexOf("(") + 1, task.lastIndexOf(")"));
      brandsCode.push(n);
    });

    var filterJson = {
      // channel: this.selectedChannelDatalist.join(","),
      // partners: this.selectedpartnerDatalist.join(","),
      region: this.selectedregionDatalist.join(","),
      state: this.selectedStateDatalist.join(","),
      city: this.selectedLocationDatalist.join(","),
      storeCode: brandsCode.join(","),
      // locationbrand: this.selectedBrandsDatalist.join(","),
      // productbrand: this.productBrandDataDatalist.join(","),
      // gender: this.genderDatalist.join(","),
      // category: this.categoryDataList.join(","),
      // subcategory: this.subCategoryDatalist.join(","),
      // type: this.typeDatalist.join(","),
      // lastname: this.lastNameDatalist.join(","),
      // season: this.seasonDatalist.join(","),
      // mrp: this.mrpDatalist.join(","),
      // articalno: this.articalNumberDatalist.join(","),
      category: this.categoryDataList.join(","),
      agCode: this.agCodeDataList.join(","),
      subcategory: this.subCategoryDatalist.join(","),
      familyName: this.familyDataList.join(","),
      setting: this.settingDataList.join(","),
      designNo: this.designDataList.join(","),
      carats: this.caratDataList.join(","),
      salePrice: this.salePriceDataList.join(","),
      modelId: this.modelIdDataList.join(","),
      quality: this.qualityDataList.join(","),
    };

    localStorage.setItem("filterJson", JSON.stringify(filterJson));

    this.headerFillters.callFilterData.next(true);
    this.lockedWindow.close();
  }

  saveFilter() {
    if (this.filterName != "") {
      const brandsCode = [];

      this.selectedStoreCodeDatalist.forEach(function (task) {
        let n = task.substring(
          task.lastIndexOf("(") + 1,
          task.lastIndexOf(")")
        );
        brandsCode.push(n);
      });
      console.log(this.categoryDataList);
      console.log(this.agCodeDataList);
      var filterJson = {
        // channel: this.selectedChannelDatalist.join(","),
        // partners: this.selectedpartnerDatalist.join(","),
        region: this.selectedregionDatalist.join(","),
        state: this.selectedStateDatalist.join(","),
        city: this.selectedLocationDatalist.join(","),
        storeCode: brandsCode.join(","),
        // locationbrand: this.selectedBrandsDatalist.join(","),
        // productbrand: this.productBrandDataDatalist.join(","),
        // gender: this.genderDatalist.join(","),
        // category: this.categoryDataList.join(","),
        // subcategory: this.subCategoryDatalist.join(","),
        // type: this.typeDatalist.join(","),
        // lastname: this.lastNameDatalist.join(","),
        // season: this.seasonDatalist.join(","),
        // mrp: this.mrpDatalist.join(","),
        // articalno: this.articalNumberDatalist.join(","),
        category: this.categoryDataList.join(","),
        agCode: this.agCodeDataList.join(","),
        subcategory: this.subCategoryDatalist.join(","),
        familyName: this.familyDataList.join(","),
        setting: this.settingDataList.join(","),
        designNo: this.designDataList.join(","),
        carats: this.caratDataList.join(","),
        salePrice: this.salePriceDataList.join(","),
        modelId: this.modelIdDataList.join(","),
        quality: this.qualityDataList.join(","),
      };

      var params = {
        Filter_name: this.filterName,
        Filter_json: JSON.stringify(filterJson),
        User_id: 1,
        Filter_id: this.editFilterId,
        Source: "",
      };

      this.headerFillters.saveFilter(params).subscribe((res) => {
        if (res["result"] == 1) {
          if (this.editFilterId == 0) {
            this.toastService.success(
              "Your filter saved successfully.",
              "Success!"
            );
            localStorage.setItem("filterJson", JSON.stringify(filterJson));
            localStorage.setItem("filterId", res["data"]);
            this.selectedFilterId = parseInt(res["data"]);
          } else {
            this.toastService.success(
              "Your filter updated successfully.",
              "Success!"
            );
            if (this.selectedFilterId == this.editFilterId) {
              localStorage.setItem("filterJson", JSON.stringify(filterJson));
              localStorage.setItem("filterId", res["data"]);
            }
          }

          this.modalService.dismissAll();
          this.getFiltersList();
          this.setFilter();
        } else {
          this.toastService.error("Something went wrong.", "Error!");
        }
      });
    } else {
      this.toastService.warning("Please enter filter name", "Warning!");
    }
  }

  getFiltersList(filterId = 0) {
    var params = {
      Filter_name: "",
      Filter_json: "",
      User_id: 1,
      Filter_id: filterId,
      Source: "",
    };

    this.headerFillters.getFilters(params).subscribe((res) => {
      if (res["result"] == 1) {
        this.filterList = res["data"];
      }
    });
  }

  removeFilter(filterId) {
    var params = {
      Filter_name: "",
      Filter_json: "",
      User_id: 1,
      Filter_id: parseInt(filterId),
      Source: "",
    };

    this.headerFillters.deleteFilter(params).subscribe((res) => {
      if (res["result"] == 1) {
        this.getFiltersList();
        this.toastService.success(
          "Your filter removed successfully.",
          "Success!"
        );
        if (filterId == this.selectedFilterId) {
          localStorage.removeItem("filterId");
          localStorage.removeItem("filterJson");
          this.modalService.dismissAll();
          this.setFilter();
        }
      } else {
        this.toastService.error("Something went wrong.", "Error!");
      }
    });
  }

  changeFilterSelection(event) {
    var filterId = event.target.value;
    this.selectedFilterId = parseInt(filterId);
    var filterData = this.filterList.filter((filter) => {
      return filter.filter_id == filterId;
    });
    if (filterData) {
      localStorage.setItem("filterJson", filterData[0]["filter_json"]);
      localStorage.setItem("filterId", filterId);
      this.setFilter();
      this.modalService.dismissAll();
    }
  }

  editFilter(content, filterId, filterName) {
    this.filterName = filterName;
    this.editFilterId = filterId;
    var filterData = this.filterList.filter((filter) => {
      return filter.filter_id == filterId;
    });
    if (filterData) {
      var filterJson = JSON.parse(filterData[0]["filter_json"]);
      //Product
      if (filterJson["productbrand"] != "") {
        this.productBrandDataDatalist = filterJson["productbrand"].split(",");
        this.productBrandDataArray = this.productBrandDataDatalist;
        this.getGenderdata();
        this.getCategorydata();
      }
      if (filterJson["gender"] != "") {
        this.genderDatalist = filterJson["gender"].split(",");
      }
      if (filterJson["category"] != "") {
        this.categoryDataList = filterJson["category"].split(",");
        this.categoryDataArray = this.categoryDataList;
        this.getSubcategorydata();
      }
      if (filterJson["subcategory"] != "") {
        this.subCategoryDatalist = filterJson["subcategory"].split(",");
        this.subcategoryDataArray = this.subCategoryDatalist;
        this.getTypedata();
      }
      if (filterJson["type"] != "") {
        this.typeDatalist = filterJson["type"].split(",");
        this.typeDataArray = this.typeDatalist;
        this.getLastnamedata();
      }
      if (filterJson["lastname"] != "") {
        this.lastNameDatalist = filterJson["lastname"].split(",");
        this.lastNameDataArray = this.lastNameDatalist;
        this.getSeasondataList();
      }
      if (filterJson["season"] != "") {
        this.seasonDatalist = filterJson["season"].split(",");
        this.seasonDataArray = this.seasonDatalist;
        this.getMrpdatalist();
      }
      if (filterJson["mrp"] != "") {
        this.mrpDatalist = filterJson["mrp"].split(",");
        this.mrpDataArray = this.mrpDatalist;
        this.getarticalnoData();
      }
      if (filterJson["articalno"] != "") {
        this.articalNumberDatalist = filterJson["articalno"].split(",");
      }

      //Location
      if (filterJson["channel"] != "") {
        this.selectedChannelDatalist = filterJson["channel"].split(",");
      }
      if (filterJson["partners"] != "") {
        this.selectedpartnerDatalist = filterJson["partners"].split(",");
      }
      if (filterJson["region"] != "") {
        this.selectedregionDatalist = filterJson["region"].split(",");
      }
      if (filterJson["state"] != "") {
        this.selectedStateDatalist = filterJson["state"].split(",");
      }
      if (filterJson["city"] != "") {
        this.selectedLocationDatalist = filterJson["city"].split(",");
      }
      if (filterJson["locationbrand"] != "") {
        this.selectedBrandsDatalist = filterJson["locationbrand"].split(",");
      }
      this.modalService.dismissAll();
      this.lockedWindow = this.modalService.open(content, {
        windowClass: "animated fadeInDown filters-modal",
        size: "lg",
      });
    }
  }

  clearFilterFields() {
    // this.productBrandDataDatalist = [];
    // this.genderDatalist = [];

    // this.typeDatalist = [];
    // this.lastNameDatalist = [];
    // this.seasonDatalist = [];
    // this.mrpDatalist = [];
    // this.articalNumberDatalist = [];
    // //Location

    // this.selectedChannelDatalist = [];
    // this.selectedpartnerDatalist = [];
    // this.selectedregionDatalist = [];
    // this.selectedStateDatalist = [];
    // this.selectedLocationDatalist = [];
    // this.selectedBrandsDatalist = [];

    // this.categoryDataList = [];
    // this.subCategoryDatalist = [];
    this.categoryDataList = [];
    this.agCodeDataList = [];
    this.subCategoryDatalist = [];
    this.familyDataList = [];
    this.settingDataList = [];
    this.designDataList = [];
    this.caratDataList = [];
    this.salePriceDataList = [];
    this.modelIdDataList = [];
    this.qualityDataList = [];
  }
}
