import { HttpClient } from "@angular/common/http";
import { environment } from "./../../environments/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class WidthService {
  GetWidthdataUrl = environment.apiUrl + "/TOCPost/GetWidthdata"; // (Post Api) All GetIbttdata list in Array
  constructor(private http: HttpClient) {}

  getWidthdata(apiParams?: any) {
    return this.http.post(this.GetWidthdataUrl, apiParams);
  }
}
