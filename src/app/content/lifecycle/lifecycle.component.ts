import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { TableApiService } from "src/app/_services/table-api.service";
const selectData = require("../../../assets//data/forms/form-elements/select.json");
@Component({
  selector: "app-lifecycle",
  templateUrl: "./lifecycle.component.html",
  styleUrls: ["./lifecycle.component.css"],
})
export class LifecycleComponent implements OnInit {
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  options = {
    close: true,
    expand: true,
    minimize: true,
    reload: true,
  };
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  rows: any;
  editing = {};
  row: any;
  rowsLifecycle: any;
  //Line Stacked Area Chart
  public lineChartData = lineChartData;
  public lineChartLabels = lineChartLabels;
  public lineChartOptions = lineChartOptions;
  public lineChartColors = lineChartColors;
  public lineChartLegend = lineChartLegend;
  public lineChartType = lineChartType;

  public singleSelectArray = selectData.singleSelectArray;
  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  @BlockUI("addRows") blockUIAddRows: NgBlockUI;
  @BlockUI("rowSelection") blockUIRowSelection: NgBlockUI;
  @BlockUI("basicProgress") blockUIBasicProgress: NgBlockUI;
  @BlockUI("coloredProgress") blockUIColoredProgress: NgBlockUI;
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Lifecycle",
          isLink: false,
          link: "/lifecycle",
        },
      ],
    };
    this.tableApiservice.getTableApiData().subscribe((Response) => {
      this.data = Response;
      this.getTabledata();
    });
  }
  getTabledata() {
    this.rows = this.data.rowsLifecycle;
    this.row = this.data.row;
  }

  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }
}

export const lineChartData: Array<any> = [
  { data: [0, 20, 11, 19, 10, 22, 9], label: "Series A" },
  { data: [28, 48, 35, 29, 46, 27, 60], label: "Series B" },
  { data: [56, 70, 55, 46, 67, 52, 70], label: "Series C" },
  { data: [60, 75, 65, 56, 77, 62, 80], label: "Series D" },
  { data: [45, 60, 10, 25, 90, 45, 60], label: "Inventory in Store" },
  { data: [35, 70, 40, 75, 80, 55, 66], label: "Inventory in Transit" },
];
export const lineChartLabels: Array<any> = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
];
export const lineChartOptions: any = {
  animation: {
    duration: 1000, // general animation timebar
    easing: "easeOutBack",
  },
  hover: {
    animationDuration: 1000, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 1000, // animation duration after a resize
  responsive: true,
  maintainAspectRatio: false,
  scales: {
    xAxes: [
      {
        display: true,
        ticks: {
          padding: 4,
        },
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        scaleLabel: {
          display: true,
          labelString: "Month",
        },
      },
    ],
    yAxes: [
      {
        display: true,
        gridLines: {
          color: "#f3f3f3",
          drawTicks: false,
        },
        ticks: {
          padding: 4,
        },
        scaleLabel: {
          display: true,
          labelString: "Value",
        },
      },
    ],
  },
};
export const lineChartColors: Array<any> = [
  {
    backgroundColor: "rgb(138,233,204,0.5)",
    borderColor: "rgb(138,233,204,1)",
    pointBackgroundColor: "rgb(138,233,204,1)",
    pointBorderColor: "rgb(138,233,204,1)",
  },
  {
    backgroundColor: "rgb(68,186,239,0.8)",
    borderColor: "rgb(168,186,239,1)",
    pointBackgroundColor: "rgb(168,186,239,1)",
    pointBorderColor: "rgb(168,186,239,1)",
  },
  {
    backgroundColor: "#ffffe0",
    borderColor: "#ffd772",
    pointBackgroundColor: "#ffd772",
    pointBorderColor: "#ffd772",
  },

  {
    backgroundColor: "#ffe8eb",
    borderColor: "#ff7e90",
    pointBackgroundColor: "#ff7e90",
    pointBorderColor: "#ff7e90",
  },
  {
    backgroundColor: "transparent",
    borderColor: "#b3b3b3",
    pointBackgroundColor: "#b3b3b3",
    pointBorderColor: "#b3b3b3",
  },
  {
    backgroundColor: "transparent",
    borderColor: "#b3b3b3",
    pointBackgroundColor: "#b3b3b3",
    pointBorderColor: "#b3b3b3",
  },
];
export const lineChartLegend = true;
export const lineChartType = "line";
