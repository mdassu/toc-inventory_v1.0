import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EditSeasonalityComponent } from "./edit-seasonality.component";
import { RouterModule } from "@angular/router";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";
import { ChartistModule } from "ng-chartist";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CardModule } from "../partials/general/card/card.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { MatchHeightModule } from "../partials/general/match-height/match-height.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ArchwizardModule } from "angular-archwizard";
import { DpDatePickerModule } from "ng2-date-picker";

@NgModule({
  declarations: [EditSeasonalityComponent],
  imports: [
    CommonModule,
    ChartistModule,
    FormsModule,
    BreadcrumbModule,
    CardModule,
    NgxDatatableModule,
    PerfectScrollbarModule,
    NgSelectModule,
    NgMultiSelectDropDownModule,
    MatchHeightModule,
    NgbModule,
    ReactiveFormsModule,
    ArchwizardModule,
    DpDatePickerModule,
    RouterModule.forChild([
      {
        path: "",
        component: EditSeasonalityComponent,
      },
    ]),
  ],
})
export class EditSeasonalityModule {}
