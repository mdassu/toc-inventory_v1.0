import { Title } from "@angular/platform-browser";
import { HeaderfiltersService } from "./../../_services/headerfilters.service";
import { IBTTService } from "./../../_services/ibtt.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { TableApiService } from "src/app/_services/table-api.service";
import { ToastrService } from "ngx-toastr";
const selectData = require("../../../assets//data/forms/form-elements/select.json");
@Component({
  selector: "app-ibtt",
  templateUrl: "./ibtt.component.html",
  styleUrls: ["./ibtt.component.css"],
})
export class IbttComponent implements OnInit {
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  options = {
    close: true,
    expand: true,
    minimize: true,
    reload: true,
  };
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  editing = {};
  rows: any;
  row: any;
  selectedChannelDatalist = [];
  selectedpartnerDatalist = [];
  selectedregionDatalist = [];
  selectedStateDatalist = [];
  selectedLocationDatalist = [];
  selectedStoreCodeDatalist = [];
  selectedBrandsDatalist = [];

  //Product tab Selected data
  productBrandDataDatalist = [];
  genderDatalist = [];
  categoryDataList = [];
  subCategoryDatalist = [];
  typeDatalist = [];
  lastNameDatalist = [];
  seasonDatalist = [];
  mrpDatalist = [];
  articalNumberDatalist = [];

  clusterDonerData: any;
  public clusterSelectArray = selectData.clusterSelectArray;
  public priorityArray = selectData.priorityArray;
  clusterNameList = [];
  pendingibtt: any;
  lockedWindow: any;
  finalRecommend: any = [];
  replenishNeeded: Object = {};
  filterJson: any = "";
  public columns = [
    { name: "Status", status: true, element: "status" },
    { name: "Replenish", status: true, element: "replenish_Needed" },
    { name: "Location Cluster", status: true, element: "location" },
    { name: "Recipient Store Code", status: true, element: "" },
    { name: "Buffer", status: true, element: "buffer" },
    { name: "Model Stone", status: true, element: "" },
    { name: "Buffer Color", status: true, element: "" },
    // { name: "SKU", status: true, element: "sku" },
    // { name: "SKU Desc", status: true, element: "sku_Desc" },
    // { name: "Origin Stock", status: true, element: "originStock" },
    // { name: "Needed", status: true, element: "replenish_Needed" },
    // { name: "VPP", status: true, element: "vpp" },
    // { name: "StatusB", status: true, element: "statusB" }, // that is the column
  ];

  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;
  @BlockUI("addRows") blockUIAddRows: NgBlockUI;
  @BlockUI("rowSelection") blockUIRowSelection: NgBlockUI;
  @BlockUI("basicProgress") blockUIBasicProgress: NgBlockUI;
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal,
    private ibttServices: IBTTService,
    private headerService: HeaderfiltersService,
    private titleService: Title,
    private toastService: ToastrService
  ) {}

  ngOnInit(): void {
    this.titleService.setTitle("IBTT | TOC-Inventory ");
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "IBTT",
          isLink: false,
          link: "/ibtt",
        },
      ],
    };

    this.getClusterData();
    this.getPriorityData();
    this.getIbttdata();

    this.headerService.callFilterDataRef.subscribe((res) => {
      if (res) {
        this.getIbttdata();
        this.headerService.callFilterData.next(false);
      }
    });
  }

  ColumnToggle() {}

  // getTabledata() {
  //   this.rows = this.data.row;
  //   this.row = this.data.row;
  // }

  // onSelect({ selected }) {
  //   this.selected.splice(0, this.selected.length);
  //   this.selected.push(...selected);
  // }
  // onSelectRow1({ selected }) {
  //   this.selected.splice(0, this.selected.length);
  //   this.selected.push(...selected);
  // }
  donerrowData;
  donerDataList(LargeModelContent, donerrowData) {
    const cluster_Name = donerrowData.cluster_Name;
    const apiParam = {
      ClusterName: cluster_Name,
      EAN: donerrowData.sku,
    };
    this.donerrowData = donerrowData;
    this.ibttServices.getIbttDonordata(apiParam).subscribe((result: any) => {
      this.clusterDonerData = result.data;
      this.lockedWindow = this.modalService.open(LargeModelContent, {
        windowClass: "animated fadeInDown",
        size: "lg",
      });
    });
  }

  chekData(row, i) {
    alert();
    row.qty = i;
  }

  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }

  getClusterData() {
    const apiParam = {};
    this.ibttServices.getClusterData(apiParam).subscribe((result: any) => {
      this.clusterSelectArray = result.data;
    });
  }

  getPriorityData() {
    const apiParam = {};
    this.ibttServices.getPriorityData(apiParam).subscribe((result: any) => {
      this.priorityArray = result.data;
    });
  }

  genArray(num) {
    return Array(parseInt(num) + 1);
  }

  IbttData = [];
  getIbttdata() {
    // this.selectedChannelDatalist = this.headerService.selectedChannelDatalist;
    // this.selectedpartnerDatalist = this.headerService.selectedpartnerDatalist;
    // this.selectedregionDatalist = this.headerService.selectedregionDatalist;
    // this.selectedStateDatalist = this.headerService.selectedStateDatalist;
    // this.selectedLocationDatalist = this.headerService.selectedLocationDatalist;
    // this.selectedStoreCodeDatalist = this.headerService.selectedStoreCodeDatalist;
    // this.selectedBrandsDatalist = this.headerService.selectedBrandsDatalist;

    // this.productBrandDataDatalist = this.headerService.productBrandDataDatalist;
    // this.genderDatalist = this.headerService.genderDatalist;
    // this.categoryDataList = this.headerService.categoryDataList;
    // this.subCategoryDatalist = this.headerService.subCategoryDatalist;
    // this.typeDatalist = this.headerService.typeDatalist;
    // this.lastNameDatalist = this.headerService.lastNameDatalist;
    // this.seasonDatalist = this.headerService.seasonDatalist;
    // this.mrpDatalist = this.headerService.mrpDatalist;
    // this.articalNumberDatalist = this.headerService.articalNumberDatalist;

    // const apiParam = {
    //   channel: this.headerService.selectedChannelDatalist.join(","),
    //   partners: this.headerService.selectedpartnerDatalist.join(","),
    //   region: this.headerService.selectedregionDatalist.join(","),
    //   state: this.headerService.selectedStateDatalist.join(","),
    //   city: this.headerService.selectedLocationDatalist.join(","),
    //   storecode: this.headerService.selectedStoreCodeDatalist.join(","),
    //   locationbrand: this.headerService.selectedBrandsDatalist.join(","),
    //   productbrand: this.headerService.productBrandDataDatalist.join(","),
    //   gender: this.headerService.genderDatalist.join(","),
    //   category: this.headerService.categoryDataList.join(","),
    //   subcategory: this.headerService.subCategoryDatalist.join(","),
    //   type: this.headerService.typeDatalist.join(","),
    //   lastname: this.headerService.lastNameDatalist.join(","),
    //   season: this.headerService.seasonDatalist.join(","),
    //   mrp: this.headerService.mrpDatalist.join(","),
    //   articalno: this.headerService.articalNumberDatalist.join(","),
    // };
    if (localStorage.getItem("filterJson")) {
      this.filterJson = localStorage.getItem("filterJson");
      var filterJson = JSON.parse(this.filterJson);
      //Product
      // if (filterJson["productbrand"] != "") {
      //   this.productBrandDataDatalist = filterJson["productbrand"].split(",");
      // }
      // if (filterJson["gender"] != "") {
      //   this.genderDatalist = filterJson["gender"].split(",");
      // }
      // if (filterJson["category"] != "") {
      //   this.categoryDataList = filterJson["category"].split(",");
      // }
      // if (filterJson["subcategory"] != "") {
      //   this.subCategoryDatalist = filterJson["subcategory"].split(",");
      // }
      // if (filterJson["type"] != "") {
      //   this.typeDatalist = filterJson["type"].split(",");
      // }
      // if (filterJson["lastname"] != "") {
      //   this.lastNameDatalist = filterJson["lastname"].split(",");
      // }
      // if (filterJson["season"] != "") {
      //   this.seasonDatalist = filterJson["season"].split(",");
      // }
      // if (filterJson["mrp"] != "") {
      //   this.mrpDatalist = filterJson["mrp"].split(",");
      // }
      // if (filterJson["articalno"] != "") {
      //   this.articalNumberDatalist = filterJson["articalno"].split(",");
      // }

      // //Location
      // if (filterJson["channel"] != "") {
      //   this.selectedChannelDatalist = filterJson["channel"].split(",");
      // }
      // if (filterJson["partners"] != "") {
      //   this.selectedpartnerDatalist = filterJson["partners"].split(",");
      // }
      // if (filterJson["region"] != "") {
      //   this.selectedregionDatalist = filterJson["region"].split(",");
      // }
      // if (filterJson["state"] != "") {
      //   this.selectedStateDatalist = filterJson["state"].split(",");
      // }
      // if (filterJson["city"] != "") {
      //   this.selectedLocationDatalist = filterJson["city"].split(",");
      // }
      // if (filterJson["locationbrand"] != "") {
      //   this.selectedBrandsDatalist = filterJson["locationbrand"].split(",");
      // }
      if (filterJson["category"] != "") {
        this.productBrandDataDatalist = filterJson["category"].split(",");
      }
      if (filterJson["agCode"] != "") {
        this.genderDatalist = filterJson["agCode"].split(",");
      }
      if (filterJson["subcategory"] != "") {
        this.categoryDataList = filterJson["subcategory"].split(",");
      }
      if (filterJson["familyName"] != "") {
        this.subCategoryDatalist = filterJson["familyName"].split(",");
      }
      if (filterJson["setting"] != "") {
        this.typeDatalist = filterJson["setting"].split(",");
      }
      if (filterJson["designNo"] != "") {
        this.lastNameDatalist = filterJson["designNo"].split(",");
      }
      if (filterJson["carats"] != "") {
        this.seasonDatalist = filterJson["carats"].split(",");
      }
      if (filterJson["salePrice"] != "") {
        this.mrpDatalist = filterJson["salePrice"].split(",");
      }
      if (filterJson["modelId"] != "") {
        this.articalNumberDatalist = filterJson["modelId"].split(",");
      }

      if (filterJson["quality"] != "") {
        this.articalNumberDatalist = filterJson["quality"].split(",");
      }

      //Location
      if (filterJson["region"] != "") {
        this.selectedregionDatalist = filterJson["region"].split(",");
      }
      if (filterJson["state"] != "") {
        this.selectedStateDatalist = filterJson["state"].split(",");
      }
      if (filterJson["city"] != "") {
        this.selectedLocationDatalist = filterJson["city"].split(",");
      }
      if (filterJson["storeCode"] != "") {
        this.selectedStoreCodeDatalist = filterJson["storeCode"].split(",");
      }
    }
    var params = {};
    if (localStorage.getItem("filterJson")) {
      params = JSON.parse(localStorage.getItem("filterJson"));
    }
    this.ibttServices.getIbttdata(params).subscribe((result: any) => {
      this.pendingibtt = result.data.ibttPendingDecision.pendingcount;
      this.rows = result.data.receipentList;
    });
  }

  getApproveRejectDepth(status) {
    const apiParam = {
      Is_Approve: status,
      Ids: String(this.donerrowData.id),
      FinalRecommend: this.finalRecommend,
    };
    this.ibttServices
      .getApproveRejectDepth(apiParam)
      .subscribe((result: any) => {
        this.getIbttdata();
        this.lockedWindow.close();
        if (result.success == 1) {
          if (status == 1) {
            this.toastService.success(
              "Your request submitted successfully.",
              "Success!"
            );
          }
        } else {
          this.toastService.error("Something went wrong.", "Error!");
        }
      });
  }

  onCheckboxChangeFn(data, event) {
    var params = {
      Ids: "1,5,8",
      Is_Approve: 1,
      FinalRecommend: [
        {
          From_Location: "Bangalore",
          To_Location: "Pali",
          item: "1234567",
          Qty: 2,
          Priority: 5,
          Status: "Approved",
        },
        {
          From_Location: "Jaipur",
          To_Location: "Kota",
          item: "7777774",
          Qty: 7,
          Priority: 2,
          Status: "Approved",
        },
      ],
    };
    if (event.target.checked) {
      this.replenishNeeded[data.id] = 0;
      var json = {
        donnerId: data.id,
        From_Location: data.originStock,
        To_Location: data.location,
        item: data.sku,
        Qty: this.replenishNeeded[data.id],
        Priority: data.priority,
        Status: data.status,
      };
      this.finalRecommend.push(json);
    } else {
      delete this.replenishNeeded[data.id];
      this.finalRecommend.map((item, key) => {
        if (item.donnerId == data.id) {
          this.finalRecommend.splice(key, 1);
        }
      });
    }
  }

  changeReplenishNeeded(donnerId) {
    this.finalRecommend.map((item) => {
      if (item.donnerId == donnerId) {
        item.Qty = parseInt(this.replenishNeeded[donnerId]);
      }
    });
  }

  clearAllFilter() {
    localStorage.removeItem("filterJson");
    localStorage.removeItem("filterId");
    this.getIbttdata();
    this.filterJson = "";
    this.headerService.callFilterData.next(true);
  }
}
