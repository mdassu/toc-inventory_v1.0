import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BlockUI, NgBlockUI } from "ng-block-ui";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"],
})
export class ProfileComponent implements OnInit {
  public breadcrumb: any;
  constructor(private formBuilder: FormBuilder) {}
  profileInfo: FormGroup;
  submitted = false;
  @BlockUI("profileInfo") blockUIProjectInfo: NgBlockUI;
  ngOnInit(): void {
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Profile",
          isLink: false,
          link: "/profile",
        },
      ],
    };
    this.profileInfo = this.formBuilder.group({
      oldPassword: ["", Validators.required],
      newPassword: ["", Validators.required],
    });
  }
  get f() {
    return this.profileInfo.controls;
  }
  onProfileSubmit() {
    this.submitted = true;

    if (this.profileInfo.invalid) {
      return;
    }
  }
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
