import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProfileComponent } from "./profile.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BreadcrumbModule } from "src/app/_layout/breadcrumb/breadcrumb.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { RouterModule } from "@angular/router";
import { MatchHeightModule } from "../partials/general/match-height/match-height.module";

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    BreadcrumbModule,
    NgbModule,
    MatchHeightModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: "",
        component: ProfileComponent,
      },
    ]),
  ],
})
export class ProfileModule {}
