import { Component, OnInit, ViewChild } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { TableApiService } from "src/app/_services/table-api.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-seasonality",
  templateUrl: "./seasonality.component.html",
  styleUrls: ["./seasonality.component.css"],
})
export class SeasonalityComponent implements OnInit {
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  rows: any;
  rowsJobs: any;
  editing = {};
  row: any;
  rowsSeasonality: any;

  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  @BlockUI("addRows") blockUIAddRows: NgBlockUI;
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Seasonality",
          isLink: false,
          link: "/seasonality",
        },
      ],
    };
    this.tableApiservice.getTableApiData().subscribe((Response) => {
      this.data = Response;
      this.getTabledata();
    });
  }
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }
  getTabledata() {
    this.rows = this.data.rowsSeasonality;
  }
}
