import { Component, OnInit, ViewChild } from "@angular/core";
import { BlockUI, NgBlockUI } from "ng-block-ui";
import {
  PerfectScrollbarComponent,
  PerfectScrollbarConfigInterface,
  PerfectScrollbarDirective,
} from "ngx-perfect-scrollbar";
import { TableApiService } from "src/app/_services/table-api.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-final-list",
  templateUrl: "./final-list.component.html",
  styleUrls: ["./final-list.component.css"],
})
export class FinalListComponent implements OnInit {
  @BlockUI("basicModals") blockUIBasicModals: NgBlockUI;
  @BlockUI("modalThemes") blockUIModalThemes: NgBlockUI;
  public breadcrumb: any;
  public config: PerfectScrollbarConfigInterface = { wheelPropagation: true };
  data: any;
  temp = [];
  selected = [];
  id: number;
  loadingIndicator: true;
  rows: any;
  rowsJobs: any;
  editing = {};
  row: any;
  rowsFinalList: any;
  jobsData: any;

  @ViewChild(PerfectScrollbarComponent)
  componentRef?: PerfectScrollbarComponent;
  @ViewChild(PerfectScrollbarDirective, { static: true })
  directiveRef?: PerfectScrollbarDirective;

  @BlockUI("addRows") blockUIAddRows: NgBlockUI;
  constructor(
    private tableApiservice: TableApiService,
    private modalService: NgbModal
  ) {}
  ngOnInit(): void {
    this.breadcrumb = {
      links: [
        {
          name: "Home",
          isLink: true,
          link: "/dashboard",
        },
        {
          name: "Final List",
          isLink: false,
          link: "/final-list",
        },
      ],
    };
    this.tableApiservice.getTableApiData().subscribe((Response) => {
      this.data = Response;
      this.getTabledata();
    });
  }
  LargeModel(LargeModelContent) {
    this.modalService.open(LargeModelContent, {
      windowClass: "animated fadeInDown",
      size: "lg",
    });
  }
  getTabledata() {
    this.rows = this.data.rowsFinalList;
    this.rowsJobs = this.data.jobsData;
  }
}
